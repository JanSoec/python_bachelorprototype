# Bachelor Prototype
This project is part of by bachelor thesis. It shows how Smart Contracts on the NEO Blockchain can be used to analyse data.

## Getting Started

### Dependencies: Linux OS, Docker, Git, Python

Install Docker:
````
terminal> sudo apt-get install docker

terminal> sudo reboot
````
Install NeoPrivateNet: https://hub.docker.com/r/cityofzion/neo-privatenet/
````
terminal> docker pull cityofzion/neo-privatenet

terminal> docker run --rm -d --name neo-privatenet -p 20333-20336:20333-20336/tcp -p 30333-30336:30333-30336/tcp cityofzion/neo-privatenet
````
Enter Docker Container and clone this Repository:
````
terminal> docker exec -it neo-privatenet /bin/bash

container> git clone https://JanSoec@bitbucket.org/JanSoec/bachelorprototype.git
````
Push the Smart Contract DemandDataFetchSC3.avm to the Blockchain:
````
container> np-prompt -p

container> open wallet neo-privnet.wallet

container> password: coz

container> import contract bachelorprototype/src/smart_contract/DemandDataFetchSC3.avm 02 02 False False

container> Contract Name: CustomName
````
Get the contract hash to be able to execute it:
````
container> contract search CustomName
# copy the contract hash without 0x prefix

container> testinvoke 'contract_hash' 'block_index'
# The block index needs to be the block with a consumer transaction
````
## Producer
The producer that wants to analyse incoming smart contract data we need to execute water_supplier.py inside the container.
````
container> python3 bachelorprototype/src/water_supplier.py
# It is now waiting for the smart contract to be executed
````

## Consumer
To execute the smart contract we need a block with consumer transactions that send demand data.
First the consumer needs to have a wallet and enough gas to invoke a transaction.
````
# clone neo-python repository
terminal> apt-get install software-properties-common python-software-properties
terminal> add-apt-repository ppa:deadsnakes/ppa
terminal> apt-get update
terminal> apt-get install python3.6 python3.6-dev python3.6-venv python3-pip libleveldb-dev libssl-dev g++
terminal> git clone https://github.com/CityOfZion/neo-python.git
terminal> cd neo-python

# create virtual environment and activate
terminal> python3.6 -m venv venv
terminal> source venv/bin/activate

# install needed packages
terminal> (venv) pip3 install wheel
terminal> (venv) pip3 install -e .
````
Create a wallet and send 100 gas from the docker container node.
````
# clone this repository for the consumer
terminal> git clone https://JanSoec@bitbucket.org/JanSoec/bachelorprototype.git
# Create a wallet. Default is /consumer.wallet | pwd: consumer01. 
# Note that the password needs to have 10 or more characters
# For more information try: bachelorprototype/src/water_consumer.py -h
terminal> bachelorprototype/src/water_consumer.py -c
terminal> np-prompt -p
terminal> open wallet consumer.wallet
# pwd: consumer01
terminal> wallet
# copy the public key of this consumer wallet
````
Send 100 gas to the consumer wallet to be able to make transactions
````
container> np-prompt -p
container> open wallet neo-privnet.wallet
# pwd: coz
container> send gas 'consumer_wallet_public_key' 100
````
After this transaction was verified the consumer has 100 gas. 

Now a new transaction with demand data can be made.
````
terminal> bachelorprototype/src/water_consumer.py -path consumer.wallet -pwd consumer01 -data 'smart_meter_value'
# Note that demand data has to be in format 'YYYY-MM-DD HH:MM:SS,MeterValue'.
````
We need at least 2 demand data transactions to calculate a gradient.
The Smart Contract takes a Block index that points to a block which should hold at least one demand data transaction.
To get the Block index of the previously made transactions:
````
terminal> np-prompt -p
terminal> tx 'transaction_hash'
# the height attribute is the block index of this transaction
````
At this point we have 2 transactions with demand data inside the blockchain and their block indices.
Last step is to start the water_supplier.py script inside the container and execute the smart contract from another client.
````
container> python3 bachelorprototype/src/water_supplier.py
````
````
client_terminal> np-prompt -p
client_terminal> open wallet consumer.wallet
# pwd: consumer01
client_terminal> testinvoke 'smart_contract_hash' 'block_index0'
client_terminal> testinvoke 'smart_contract_hash' 'block_index1'
````
As this invocation is verified by the container nodes it's output is send to the water_supplier.py script running on the container.
After the second transaction has invoked water_supplier.py the gradient and y-intercept are printed to the console.