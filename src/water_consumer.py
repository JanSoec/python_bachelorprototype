#!/usr/bin/env python3
"""
This script can be used to create consumer wallets and send demand data.
"""

import time
import datetime
import argparse

from neo.Wallets.utils import to_aes_key
from neo.Implementations.Wallets.peewee.UserWallet import UserWallet
from neo.Implementations.Blockchains.LevelDB.LevelDBBlockchain import LevelDBBlockchain
from neo.Core.Blockchain import Blockchain
from neo.Prompt.Commands.Send import construct_and_send
from neo.Core.TX.TransactionAttribute import TransactionAttribute, TransactionAttributeUsage
from neo.Network.NodeLeader import NodeLeader
from twisted.internet import reactor, task
from neo.Settings import settings

DEFAULT_WALLET_PATH = "consumer.wallet"
DEFAULT_WALLET_PWD = "consumer01"


class SmartMeterBCCommunicator(object):
    """
    This class can be used by customers to create a wallet and send smart meter data to the blockchain.
    The data is saved inside the attribute slot of the transaction.
    Therefore only the amount of gas needed for this transaction is used in this process.
    """
    start_height = None
    start_dt = None
    _walletdb_loop = None

    wallet_fn = None
    wallet_pwd = None
    min_wait = None
    sm_data = None
    create = None

    def __init__(self, args):
        self.wallet_fn = DEFAULT_WALLET_PATH if args.wallet_path is None else args.wallet_path
        self.wallet_pwd = DEFAULT_WALLET_PWD if args.wallet_password is None else args.wallet_password
        self.min_wait = 1 if args.wait_time is None else args.wait_time
        self.sm_data = -1 if args.sm_data is None else args.sm_data
        self.create = args.create

        self.start_height = Blockchain.Default().Height
        self.start_dt = datetime.datetime.utcnow()

    def quit(self):
        print('Shutting down.  This may take a bit...')
        self.go_on = False
        Blockchain.Default().Dispose()
        reactor.stop()
        NodeLeader.Instance().Shutdown();

    def wait_for_tx(self, tx, max_seconds=300):
        """ Wait for tx to show up on blockchain """
        found_tx = False
        sec_passed = 0
        while not found_tx and sec_passed < max_seconds:
            _tx, height = Blockchain.Default().GetTransaction(tx.Hash)
            if height > -1:
                found_tx = True
                continue
            print("Waiting for tx {} to show up on blockchain...".format(tx.Hash.ToString()))
            time.sleep(3)
            sec_passed += 3
        if found_tx:
            return True
        else:
            print("Transaction was relayed but never accepted by consensus node")
            return False
    
    def send_data(self, data, to_address="AK2nJJpJr6o664CWJKi1QRXjqeic2zRp8y", asset="gas", asset_amount="0.001"):
        """ Send SmartMeter value to the blockchain """
        if self.wallet is None:
            print("Wallet could not be found.")
            return

        print("Sending " + asset_amount + " " + asset + " to " + to_address + ".")

        # Add smart meter data to the attribute field of this transaction
        attribute_object = TransactionAttribute(
            usage=TransactionAttributeUsage.Description, data=str(data).encode())
        attribute = "--tx-attr=" + str(attribute_object.ToJson())

        # Make transaction
        tx = construct_and_send(None, self.wallet, [asset, to_address, asset_amount, attribute], prompt_password=False)
        if not tx:
            print("Something went wrong, no tx")
            return
        self.wait_for_tx(tx)
        print("Transaction success.")
        
    def open_wallet(self):
        """ Open a wallet at given path with given password """
        print("Opening wallet %s" % self.wallet_fn)
        self.wallet = UserWallet.Open(self.wallet_fn, to_aes_key(self.wallet_pwd))
        if self.wallet is None:
            print("Couldn't open the wallet.")
            return
        self.wallet.ProcessBlocks()
        self._walletdb_loop = task.LoopingCall(self.wallet.ProcessBlocks)
        self._walletdb_loop.start(1)
        print("Wallet is now open.")
        
    def create_wallet(self):
        """ Create a wallet at given path with given password """
        print("Creating wallet at " + self.wallet_fn + " pwd: " + self.wallet_pwd)
        self.wallet = UserWallet.Create(self.wallet_fn, to_aes_key(self.wallet_pwd))
        if self.wallet is None:
            print("Couldn't create the wallet.")
            return
        self.wallet.ProcessBlocks()
        self._walletdb_loop = task.LoopingCall(self.wallet.ProcessBlocks)
        self._walletdb_loop.start(1)
        self.wallet.Rebuild()
        print("Rebuilding wallet. Please wait 20 seconds.")
        time.sleep(20)
        print('Wallet was created at ' + self.wallet_fn + ". Use password: " + self.wallet_pwd)
    
    def run(self):
        dbloop = task.LoopingCall(Blockchain.Default().PersistBlocks)
        dbloop.start(.1)
        Blockchain.Default().PersistBlocks()

        while Blockchain.Default().Height < 2:
            print("Waiting for chain to sync...")
            time.sleep(1)

        if self.create:
            self.create_wallet()
        else:
            self.open_wallet()
            self.send_data(self.sm_data)
        
        self.quit()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Sends SmartMeter data to the neo blockchain.')
    parser.add_argument("-c", "--create", help="Use if you want to create a wallet.", action="store_true")
    parser.add_argument('-path', '--wallet_path', help='The file path for the created wallet', type=str)
    parser.add_argument('-pwd', '--wallet_password', help='The password for the wallet to be created for this consumer.', type=str)
    parser.add_argument('-data', '--sm_data', help='The demand data value read from a SmartMeter.\nSingle entry: YYYY-MM-DD HH:MM:SS,Value\nMultiple entries format: YYYY-MM-DD HH:MM:SS,Value0;YYYY-MM-DD HH:MM:SS,Value1', type=str)
    parser.add_argument('-wait', '--wait_time', help='Time to wait between data sends.', type=float)
    args = parser.parse_args()

    settings.setup_privnet()
    print("Blockchain DB path:", settings.chain_leveldb_path)

    blockchain = LevelDBBlockchain(settings.chain_leveldb_path)
    Blockchain.RegisterBlockchain(blockchain)

    reactor.suggestThreadPoolSize(15)
    NodeLeader.Instance().Start()
    pc = SmartMeterBCCommunicator(args)
    if args.wallet_path is None or len(args.wallet_path) >= 10:
        reactor.callInThread(pc.run)
        reactor.run()
    else:
        print('Password must have 10 or more characters')
        pc.quit()
