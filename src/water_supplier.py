import threading
from time import sleep
from analyse.regression import RegressionAnalyser
from analyse.smart_meter_data_format import SmartContractData, SmartContractParser

from twisted.internet import reactor, task

from neo.contrib.smartcontract import SmartContract
from neo.Network.NodeLeader import NodeLeader
from neo.Core.Blockchain import Blockchain
from neo.Implementations.Blockchains.LevelDB.LevelDBBlockchain import LevelDBBlockchain
from neo.Settings import settings


smart_contract = SmartContract("1ae946bf02a66c54b1f4994d9aa2b5bf09e20f98")
analyser = RegressionAnalyser()
parser = SmartContractParser()


# def plot_trend(data_array):
#     # Plot trend
#     dates = [element.x_value for element in data_array]
#     values = [element.y_value for element in data_array]
#
#     for i in range(0, 12):
#         gradient, y_intercept = analyser.calculate_gradient(data_array[6*i:6*(i+1)])
#         x = []
#         y = []
#         for element in data_array[6*i:6*(i+1)]:
#             x.append(element.x_value)
#             y.append((element.x_value.timestamp() * gradient) + y_intercept)
#         plt.plot(x, y, linewidth=2, color='b')
#
#     plt.subplots_adjust(bottom=0.2)
#     plt.xticks(rotation=25)
#     ax = plt.gca()
#     xfmt = md.DateFormatter('%Y-%m-%d %H:%M:%S')
#     ax.xaxis.set_major_formatter(xfmt)
#     plt.plot(dates, values)
#     plt.xlabel('Datum')
#     plt.ylabel('Wasserbedarf')
#     plt.title('Trendanalyse mittels Regressionsgerade')
#     plt.show()


@smart_contract.on_execution
def sc_notify(event):
    """
    Callback for smart contract execution. Start of offchain operations
    :param event: Output of the smart contract execution.
    """
    if not len(event.event_payload):
        print("No Event Payload")
        return

    # Analyse the event payload
    print("Smart Contract was executed. EventType: " + event.event_type + "; Success: " + str(event.execution_success))
    data = parser.parse_data(input_data=event.event_payload[0])
    print("Parsed data: ")
    for entry in data:
        print(str(entry))
    if len(data) > 1:  # can not calculate gradient of only a single entry
        gradient, intercept = analyser.calculate_gradient(data)
        print("Gradient: " + str(gradient) + "x; Y-Intercept: " + str(intercept))
        if gradient > 1:
            print('Demand is rising at a rate of: ' + str(gradient))
        elif gradient < -1:
            print('Demand is dropping at a rate of: ' + str(gradient))


def custom_background_code():
    """
    Code that is run while waiting for the smart contract execution.
    """
    while True:
        print("Block" + str(Blockchain.Default().Height) + "/" + str(Blockchain.Default().HeaderHeight))
        sleep(15)


def main():
    # Use PrivNet
    settings.setup_privnet()

    # Setup the blockchain
    blockchain = LevelDBBlockchain(settings.chain_leveldb_path)
    Blockchain.RegisterBlockchain(blockchain)
    dbloop = task.LoopingCall(Blockchain.Default().PersistBlocks)
    dbloop.start(.1)
    NodeLeader.Instance().Start()

    parser.set_blockchain(Blockchain.Default())
    # Disable smart contract events for external smart contracts
    settings.set_log_smart_contract_events(False)

    # Start a thread with custom code
    d = threading.Thread(target=custom_background_code)
    d.setDaemon(True)  # daemonizing the thread will kill it when the main thread is quit
    d.start()

    # Run all the things (blocking call)
    print("Everything setup and running. Waiting for events...")
    reactor.run()
    print("Shutting down.")


if __name__ == "__main__":
    main()
