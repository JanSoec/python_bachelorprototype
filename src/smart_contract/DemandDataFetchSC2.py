from boa.interop.Neo.Blockchain import GetBlock, GetHeader, GetTransaction
# Block imports
from boa.interop.Neo.Block import Block, GetTransaction, GetTransactionCount
from boa.interop.Neo.Header import GetTimestamp
# Transaction imports
from boa.interop.Neo.Transaction import GetAttributes, GetOutputs, GetInputs
from boa.interop.Neo.Attribute import GetData
from boa.interop.Neo.Output import GetScriptHash
from boa.interop.Neo.Input import GetInputHash


def Main(block_index):
    # Get basic references
    block = GetBlock(block_index)
    header = GetHeader(block_index)
    timestamp = GetTimestamp(header)
    tx_index = GetTransactionCount(block)
    
    # Add block specific data
    result = []
    result.append(block_index)
    result.append(timestamp)
  
    # loop over all transactions of a given block
    while tx_index > 0: 
        tx_index = tx_index - 1
        
        # Get receiver address
        transaction = GetTransaction(block, tx_index)
        
        # Get sender address
        inputs = GetInputs(transaction)
        prev_tx_hash = GetInputHash(inputs)
        prev_tx = GetTransaction(prev_tx_hash)
        sender = GetScriptHash(prev_tx)
        
        # Get transaction attributes
        attributes = GetAttributes(transaction)
        attr_index = len(attributes)
        
        # Add transaction specific data
        tx_entry = []
        tx_entry.append(tx_index)
        tx_entry.append(sender)
        tx_entry.append()
        
        # Loop over all transaction attributes
        while attr_index > 0: # loop through all attributes of a given transaction
            attr_index = attr_index - 1
            attr_data = GetData(attributes[attr_index])
            
            # Add attribute specific data
            entry = []
            entry.append(attr_index)
            entry.append(attr_data)
            tx_index.append(entry)
        
        # Add transaction entry to the result array
        result.append(tx_entry)
    return result
