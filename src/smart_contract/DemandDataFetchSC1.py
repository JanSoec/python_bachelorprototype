from boa.interop.Neo.Blockchain import GetBlock
from boa.interop.Neo.Block import Block, GetTransactions, GetTransaction, GetTransactionCount
from boa.interop.Neo.Transaction import GetAttributes, GetOutputs
from boa.interop.Neo.Attribute import GetUsage, GetData
from boa.interop.Neo.Runtime import GetTime
from boa.interop.Neo.Output import GetAssetId, GetValue, GetScriptHash


def Main(block_index):
  result = []
  block = GetBlock(block_index)
  tx_index = GetTransactionCount(block)
  while tx_index > 0:
    tx_index = tx_index - 1
    transaction = GetTransaction(block, tx_index)
    attributes = GetAttributes(transaction)
    attr_index = len(attributes)
    while attr_index > 0:
      attr_index = attr_index -1
      entry = []
      entry.append(block_index)
      entry.append(tx_index)
      entry.append(attr_index)
      entry.append(GetData(attributes[attr_index]))
      result.append(entry)
  return result
