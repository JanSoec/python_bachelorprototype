import struct
from datetime import datetime


class SmartContractData:
    """
    This class represents a data format for demand data stored outside of the blockchain.
    It is used for regression and leakage analysis.
    """

    def __init__(self, block_index, tx_index, blocktime, value, address):
        """
        :param block_index: The block index of this transaction
        :param tx_index: The Transaction index inside of the block
        :param blocktime: Timestamp of the Block
        :param value: Demand data value. Format: 2014-01-01 00:00:00,100.00
        :param address: Public address of the consumer
        """
        self.block_index = int(block_index)
        self.transaction_index = int(tx_index)
        self.blocktime = blocktime
        if isinstance(value, str):
            split = value.split(',')    # format: YYYY-MM-DD HH:MM:SS,MeterValue - e.g. 2010-01-01 00:00:00,100.0
            self.x_value = datetime.strptime(split[0][2:], '%Y-%m-%d %H:%M:%S')     # extract date
            self.y_value = float(split[1][:-1])     # extract meter value
        else:
            print('ValueError: Value needs to be e.g. 2010-01-01 00:00:00,100.0')
        self.address = str(address)

    def __str__(self):
        return "Block_Index: " + str(self.block_index) \
               + "; TX_Index: " + str(self.transaction_index) \
               + "; Blocktime: " + str(self.blocktime) \
               + "; xVal: " + str(self.x_value) \
               + "; yVal: " + str(self.y_value) \
               + "; Address: " + str(self.address)


class SmartContractParser:
    """
    Parser to convert the byte data from smart contract execution into above data format.
    Works with DemandDataFetchSC3.py.
    """

    def __init__(self):
        self.data = []  # Array of SmartContractData entries

    def set_blockchain(self, blockchain):
        self.blockchain = blockchain  # reference to a neo LevelDB instance of the NEO Blockchain

    def parse_data(self, input_data):
        """
        Parses a given smart contract payload into SmartContractData format.
        :param input_data: The smart contract payload
        :return: An Array of all parsed data entries
        """
        block_index = struct.unpack("h", input_data[0])[0]
        block_time = input_data[1]
        for i in range(2, len(input_data)):  # Starts at 2 because of the way DemandDataFetchSC3 returns values.
            try:
                transaction = input_data[i]
                transaction_index = transaction[0]
                demand_value = str(bytes.fromhex(transaction[2].decode('utf-8')))
                address = self.get_public_address(block_index, transaction[0])

                if isinstance(demand_value, str):
                    demand_value = demand_value[2:]     # Get rid of b' prefix
                    demand_value = demand_value[:-1]    # Get rid of ' appendage
                    split = demand_value.split(';')    # format: YYYY-MM-DD HH:MM:SS,MeterValue
                    for entry in split:
                        self.data.append(SmartContractData(block_index, transaction_index, block_time, entry, address))
            except UnicodeDecodeError:
                print("No Demand Data At Index: " + str(i))
        return self.data

    def get_public_address(self, block_index, transaction_index):
        """
        Gets the public address of a consumer.
        :param block_index: Block to search for the transaction.
        :param transaction_index: Transaction index inside the given block.
        :return: The public address as a string
        """
        block = self.blockchain.GetBlockByHeight(block_index)

        if block is None or len(block.LoadTransactions()) < transaction_index:
            print("Transaction couldn't be found. " + str(block_index) + ", " + str(transaction_index))
            return None

        output = block.LoadTransactions()[transaction_index]
        js_data = output.ToJson()
        return js_data['vout'][1]['address']

