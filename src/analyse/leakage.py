
def print_alert(alert_list, alert_count):
    # create output message
    leakage_alert = 'Found ' + str(alert_count) + ' times. Possible leakage on meter-id: '
    for i, alert in enumerate(alert_list):
        leakage_alert += alert
        if i < len(alert_list)-1:
            leakage_alert += ', '
    # print outputs
    print(leakage_alert)


class LeakageDetector:
    """
    This class is used to detect leakage candidates from a given SmartContractData Array.
    """
    def detect(self, sc_data_array, sum_demand_dict, min_hour, max_hour, household_count=105, alert_count=9):
        """

        :param sc_data_array: SmartContractData Array
        :param sum_demand_dict: Dictionary in MeterID : TotalDemandSum format
        :param min_hour: Lower time limit of data analysis
        :param max_hour: Upper time limit of data analysis
        :param household_count: Amount of individual households contained in sc_data_array
        :return: A list of all addresses and another list with all SmartContractDataEntries that posses a leak
        """

        daily_dict = dict()

        for entry in sc_data_array:
            current_date = entry.x_value
            meter_value = entry.y_value

            y_avg = float(sum_demand_dict[str(current_date)]) / household_count
            # print('known_leak' + ',' + str(current_date) + ',' + str(y_avg + 0.005))

            if meter_value > y_avg and min_hour < current_date.hour < max_hour:
                if current_date.day not in daily_dict:
                    daily_dict[current_date.day] = []
                daily_dict[current_date.day].append(entry)

        # find leakage candidates
        alert_list = []
        candidate_dict = dict()
        for day, data in daily_dict.items():
            for entry in data:
                meter_id = entry.address
                if meter_id not in candidate_dict:
                    candidate_dict[meter_id] = 0
                candidate_dict[meter_id] += 1

        for meter_id, count in candidate_dict.items():
            if count == alert_count:
                alert_list.append(meter_id)
        print_alert(alert_list, alert_count)

        output = dict()
        for entry in sc_data_array:
            if entry.address in alert_list:
                if entry.address not in output:
                    output[entry.address] = []
                output[entry.address].append(entry)

        return alert_list, list(output.values())
