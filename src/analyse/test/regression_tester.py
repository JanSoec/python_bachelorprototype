import os
import matplotlib.pyplot as plt
import matplotlib.dates as md
from datetime import datetime
from src.analyse.regression import RegressionAnalyser
from src.analyse.smart_meter_data_format import SmartContractData


def get_searched_dates(searched_year, searched_month, searched_day):
    correct_dates = []
    date = searched_year + '-' + searched_month + '-' + searched_day
    for h in range(0, 24):
        hour = ' '
        if h < 10:
            hour += '0'
        hour += str(h) + ':00:00'
        correct_dates.append(date + hour)
    return correct_dates


def get_hourly_sum(hourly_sum_dir, dates_to_analyse=None):
    if dates_to_analyse is None:
        dates_to_analyse = ['2016-06-01', '']
    if not os.path.exists(hourly_sum_dir):
        print("Error: Input Directory not found.")

    output_dict = dict()
    if dates_to_analyse is not None:
        for entry in dates_to_analyse:
            split = entry.split('-')
            year = split[0]
            month = split[1]
            day = split[2]

            for entry in get_searched_dates(searched_year=year, searched_month=month, searched_day=day):
                output_dict[entry] = 0

    for filename in os.listdir(hourly_sum_dir):
        if os.path.isdir(hourly_sum_dir + filename):
            continue
        in_file = open(hourly_sum_dir + filename, 'r')
        for i, line in enumerate(in_file.readlines()):
            try:
                split_line = line.split(',')
                date = split_line[0]
                meter_value = float(split_line[1])
            except IndexError:
                continue

            if dates_to_analyse is None:
                if date in output_dict:
                    output_dict[date] += meter_value
                else:
                    output_dict[date] = meter_value
            elif date in output_dict:
                output_dict[date] += meter_value
    return output_dict


if __name__ == '__main__':
    """
    This Test is supposed to be run using the SmartH20 dataset from https://github.com/The-SmartH2O-project/datasets
    """

    dataset_path = '../smarth2o_anonymized_consumptions.csv'    # Path to the SmartH2O dataset
    sub_dataset_dir = '../dataset-split/2016/'    # Path to the directory you want to extract each dataset per smart meter
    hourly_difference_dir = '../dataset-split/2016/hourly_difference/'  # Path to the directory that should contain consumptions
    hourly_sum_dir = '../dataset-split/2016/hourly_sum/'
    year = '2016'   # The year to be analysed
    min_sub_dataset_size = 8000     # Minimum needed data entries per household

    print('Getting hourly difference sum...')
    total_demand_dict = get_hourly_sum(hourly_sum_dir, dates_to_analyse=['2016-06-01', '2016-06-02', '2016-06-03'])
    print('Data extraction success.')
    for key, value in total_demand_dict.items():
        print(str(key) + ': ' + str(value))

    print('Preparing to plot...')
    dates = [datetime.strptime(key, '%Y-%m-%d %H:%M:%S') for key in total_demand_dict.keys()]
    values = [float(value) for value in total_demand_dict.values()]
    data_array = []
    for i in range(len(dates)):
        data_array.append(
            SmartContractData(i, 1, dates[i].timestamp(), str(dates[i]) + ',' + str(values[i]), 'address'))
    analyser = RegressionAnalyser()

    print('Calculating gradient and y intercept.')
    # Plot trend
    for i in range(0, 12):
        gradient, y_intercept = analyser.calculate_gradient(data_array[6*i:6*(i+1)])
        x = []
        y = []
        for element in data_array[6*i:6*(i+1)]:
            x.append(element.x_value)
            y.append((element.x_value.timestamp() * gradient) + y_intercept)
        plt.plot(x, y, linewidth=2, color='b')

    # vertical lines
    day_one_end = datetime.strptime('2016-06-02 00:00:00', '%Y-%m-%d %H:%M:%S')
    day_two_end = datetime.strptime('2016-06-03 00:00:00', '%Y-%m-%d %H:%M:%S')
    plt.axvline(x=day_one_end, color=(0.8, 0.8, 0.8))
    plt.axvline(x=day_two_end, color=(0.8, 0.8, 0.8))

    print('Plotting.')
    plt.subplots_adjust(bottom=0.2)
    plt.xticks(rotation=25)
    ax = plt.gca()
    xfmt = md.DateFormatter('%Y-%m-%d %H:%M:%S')
    ax.xaxis.set_major_formatter(xfmt)
    plt.plot(dates, values)
    plt.xlabel('Zeit')
    plt.ylabel('Wasserverbrauch')
    plt.title('Trendanalyse mittels Regressionsgerade')
    print('Done.')
    plt.show()
