import matplotlib.pyplot as plt
import matplotlib.dates as md
from src.analyse.leakage import LeakageDetector
import os
from datetime import datetime
from src.analyse.smart_meter_data_format import SmartContractData
from src.analyse.test.regression_tester import get_hourly_sum


def get_leakage_data(input_dir):
    if not os.path.exists(input_dir):
        print("Error: Input Directory not found.")

    correct_dates = get_hours_of_dates('2016', '06', '01')
    for entry in get_hours_of_dates('2016', '06', '02'):
        correct_dates.append(entry)
    for entry in get_hours_of_dates('2016', '06', '03'):
        correct_dates.append(entry)

    output = []
    for filename in os.listdir(input_dir):
        if os.path.isdir(input_dir + filename):
            continue
        in_file = open(input_dir + filename, 'r')
        for i, line in enumerate(in_file.readlines()):
            try:
                split_line = line.split(',')

                meter_id = split_line[0]
                date = datetime.strptime(split_line[1], '%Y-%m-%d %H:%M:%S')
                meter_value = float(split_line[2])
            except IndexError:
                continue
            if str(date) in correct_dates:
                entry = SmartContractData(i, 1, date.timestamp(), str(date) + ',' + str(meter_value), meter_id)
                output.append(entry)
    return output


def get_hours_of_dates(searched_year, searched_month, searched_day):
    """
    :param searched_year:
    :param searched_month:
    :param searched_day:
    :return:
    """
    correct_dates = []
    date = searched_year + '-'+searched_month+'-'+searched_day
    for h in range(0, 24):
        hour = ' '
        if h < 10:
            hour += '0'
        hour += str(h) + ':00:00'
        correct_dates.append(date + hour)
    return correct_dates


def get_plotable_data(sc_data_input):
    x = []
    y = []
    for entry in sc_data_input:
        x.append(entry.x_value)
        y.append(entry.y_value)
    return x, y


if __name__ == '__main__':
    dataset_path = '../smarth2o_anonymized_consumptions.csv'
    sub_dataset_dir = '../dataset-split/2016_split/'
    sub_difference_dir = '../dataset-split/2016/hourly_difference/'
    sub_sum_dir = '../dataset-split/2016/hourly_sum/'
    year = '2016'
    min_sub_dataset_size = 8000

    print('Calculating hourly difference sum...')
    out_dict = get_hourly_sum(sub_sum_dir, dates_to_analyse=['2016-06-01', '2016-06-02', '2016-06-03'])

    leakage_data = get_leakage_data(sub_difference_dir)     # dataset into SmartMeterData format
    min_hour = 0    # start hour for leakage search
    max_hour = 4    # end hour for leakage search
    total_entries = 106     # amount of houses

    print('Leakage analysis...')
    dect = LeakageDetector()
    alert_list, leakage_candidates_9 = dect.detect(leakage_data, out_dict, min_hour, max_hour, total_entries, 9)
    alert_list, leakage_candidates_8 = dect.detect(leakage_data, out_dict, min_hour, max_hour, total_entries, 8)

    output = dict()
    for entry in leakage_data:
        if entry.address not in alert_list:
            if entry.address not in output:
                output[entry.address] = []
            output[entry.address].append(entry)

    print('Plotting.')
    plt.subplots_adjust(bottom=0.2)
    plt.xticks(rotation=25)
    ax = plt.gca()
    xfmt = md.DateFormatter('%Y-%m-%d %H:%M:%S')
    ax.xaxis.set_major_formatter(xfmt)

    x, y = get_plotable_data(leakage_candidates_9[0])
    plt.plot(x, y,  label='23')#color='red',
    x, y = get_plotable_data(leakage_candidates_9[1])
    plt.plot(x, y, label='60') #color='orangered',
    x, y = get_plotable_data(leakage_candidates_9[2])
    plt.plot(x, y, color='b', label='known leak')       # different color, as this is also the average

    x, y = get_plotable_data(leakage_candidates_8[0])
    plt.plot(x, y,  label='233') #color='orange',
    x, y = get_plotable_data(leakage_candidates_8[1])
    plt.plot(x, y,  label='5')# color='darkgoldenrod',
    plt.legend(loc='upper right')

    start0 = datetime.strptime('2016-06-01 00:00:00', '%Y-%m-%d %H:%M:%S')
    end0 = datetime.strptime('2016-06-01 04:00:00', '%Y-%m-%d %H:%M:%S')
    ax.fill_between([start0, start0], -0.01, 0.6, facecolor='blue', alpha=0.2)

    start1 = datetime.strptime('2016-06-02 00:00:00', '%Y-%m-%d %H:%M:%S')
    end1 = datetime.strptime('2016-06-02 04:00:00', '%Y-%m-%d %H:%M:%S')
    ax.fill_between([start1, end1], -0.01, 0.6, facecolor='blue', alpha=0.2)

    start2 = datetime.strptime('2016-06-03 00:00:00', '%Y-%m-%d %H:%M:%S')
    end2 = datetime.strptime('2016-06-03 04:00:00', '%Y-%m-%d %H:%M:%S')
    ax.fill_between([start2, end2], -0.01, 0.6, facecolor='blue', alpha=0.2)

    plt.xlabel('Zeit')
    plt.ylabel('Wasserverbrauch')
    plt.title('Trendanalyse mittels Regressionsgerade')
    print('Done.')
    plt.show()
