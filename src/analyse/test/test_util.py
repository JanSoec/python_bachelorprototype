def create_sub_datasets(dataset_path, split_path, searched_year='2016'):
    dataset_file = open(dataset_path, 'r')
    split_file = None
    current_meter = ''

    for line in dataset_file.readlines():
        split_line = line.split(',')

        index = split_line[0]
        meter_id = split_line[1]
        timestamp = split_line[2]
        meter_value = str(split_line[3])

        if current_meter != meter_id:
            current_meter = meter_id
            if split_file is not None:
                split_file.close()
            split_file = open(split_path + meter_id + '.txt', 'w')

        if timestamp.startswith(searched_year):
            split_file.write(meter_id + ',' + timestamp + ',' + meter_value)
    split_file.close()


def file_len(fname):
    return sum(1 for line in open(fname))


def remove_small_datasets(needed_size, input_dir='dataset-split/'):
    if not os.path.exists(input_dir):
        print("Error: Input Directory not found.")

    for filename in os.listdir(input_dir):
        if os.path.isdir(input_dir + filename):
            continue
        if file_len(input_dir + filename) < needed_size:
            os.remove(input_dir + filename)


def calculate_hourly_difference(input_dir, output_dir):
    if not os.path.exists(input_dir):
        print("Error: Input Directory not found.")
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for filename in os.listdir(input_dir):
        if os.path.isdir(input_dir + filename):
            continue
        in_file = open(input_dir + filename, 'r')
        out_file = open(output_dir + filename, 'w')
        prev_value = 0
        for i, line in enumerate(in_file.readlines()):
            split_line = line.split(',')

            meter_id = split_line[0]
            timestamp = split_line[1]
            meter_value = float(split_line[2])

            if i > 0:
                out_file.write(meter_id + ',' + timestamp + ',' + str(round(meter_value - prev_value, 3)) + '\n')

            prev_value = meter_value
        out_file.close()


def calculate_hourly_sum(input_dir, output_dir):
    if not os.path.exists(input_dir):
        print("Error: Input Directory not found.")
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    output_dict = dict()

    for filename in os.listdir(input_dir):
        if os.path.isdir(input_dir + filename):
            continue
        in_file = open(input_dir + filename, 'r')
        for i, line in enumerate(in_file.readlines()):
            try:
                split_line = line.split(',')

                meter_id = split_line[0]
                date = split_line[1]
                meter_value = float(split_line[2])
            except IndexError:
                continue

            if date in output_dict:
                output_dict[date] += meter_value
            else:
                output_dict[date] = meter_value
        in_file.close()

    out_file = open(output_dir + 'hourly_sum.txt', 'w')
    for key, value in output_dict.items():
        out_file.write(str(key) + ',' + str(value) + '\n')
    out_file.close()


if __name__ == '__main__':
    """
    This script extracts data from the smarth2o dataset into smaller chunks. 
    These smaller datasets can then be used by regression_tester.py and leakage_tester.py.
    """
    dataset_path = '../smarth2o_anonymized_consumptions.csv'
    sub_dataset_dir = '../dataset-split/2016/'
    sub_hourly_difference_dir = '../dataset-split/2016/hourly_difference/'
    sub_hourly_sum_dir = '../dataset-split/2016/hourly_sum/'
    year = '2016'
    min_sub_dataset_size = 8000

    print('Creating a separate file for each smart meter...')
    create_sub_datasets(dataset_path, sub_dataset_dir, searched_year=year)
    print('Removing datasets with less than ' + str(min_sub_dataset_size) + ' entries...')
    remove_small_datasets(min_sub_dataset_size, sub_dataset_dir)
    print('Calculating hourly differences from ' + sub_dataset_dir + '. Output at: ' + sub_hourly_difference_dir)
    calculate_hourly_difference(sub_dataset_dir, sub_hourly_difference_dir)
    print('Calculating hourly sum from ' + sub_dataset_dir + '. Output at: ' + sub_hourly_sum_dir)
    calculate_hourly_sum(sub_hourly_difference_dir, sub_hourly_sum_dir)
