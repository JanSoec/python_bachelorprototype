import math
from datetime import datetime


def calculate_sum_x(sc_data_array):
    total = 0
    for element in sc_data_array:
        total += int(element.x_value)
    return total


def calculate_sum_y(sc_data_array):
    total = 0
    for element in sc_data_array:
        total += int(element.y_value)
    return total


def calculate_average_x(sc_data_array):
    return calculate_sum_x(sc_data_array) / len(sc_data_array)


def calculate_average_y(sc_data_array):
    return calculate_sum_y(sc_data_array) / len(sc_data_array)


def calculate_variance_x(sc_data_array):
    variance_x = 0
    average = calculate_average_x(sc_data_array)
    for element in sc_data_array:
        variance_x += (element.x_value - average)**2
    return variance_x / len(sc_data_array)


def calculate_variance_y(sc_data_array):
    variance_y = 0
    average = calculate_average_y(sc_data_array)
    for element in sc_data_array:
        variance_y += (element.y_value - average)**2
    return variance_y / len(sc_data_array)


def standard_deviation_x(sc_data_array):
    return math.sqrt(calculate_variance_x(sc_data_array))


def standard_deviation_y(sc_data_array):
    return math.sqrt(calculate_variance_y(sc_data_array))


def calculate_minimum_x(sc_data_array):
    minimum = sc_data_array[0].x_value
    for element in sc_data_array:
        if element.x_value < minimum:
            minimum = element.x_value
    return minimum


def calculate_minimum_y(sc_data_array):
    minimum = sc_data_array[0].y_value
    for element in sc_data_array:
        if element.y_value < minimum:
            minimum = element.y_value
    return minimum


def calculate_maximum_x(sc_data_array):
    maximum = 0
    for element in sc_data_array:
        if element.x_value > maximum:
            maximum = element.x_value
    return maximum


def calculate_maximum_y(sc_data_array):
    maximum = 0
    for element in sc_data_array:
        if element.x_value > maximum:
            maximum = element.x_value
    return maximum


def calculate_range_x(sc_data_array):
    return calculate_maximum_x(sc_data_array) - calculate_minimum_x(sc_data_array)


def calculate_range_y(sc_data_array):
    return calculate_maximum_y(sc_data_array) - calculate_minimum_y(sc_data_array)


# from regression import SmartContractData
# if __name__ == '__main__':

#     data_array = [SmartContractData(1, 1, 15, 20, "Address1", x_divisor=1),
#                   SmartContractData(2, 1, 70, 40, "Address2", x_divisor=1),
#                   SmartContractData(3, 1, 25, 60, "Address3", x_divisor=1),
#                   SmartContractData(4, 1, 50, 80, "Address4", x_divisor=1),
#                   SmartContractData(5, 1, 30, 100, "LeakAddress1", x_divisor=1),
#                   SmartContractData(6, 1, 100, 120, "LeakAddress2", x_divisor=1),
#                   SmartContractData(7, 1, 12, 140, "LeakAddress3", x_divisor=1),
#                   SmartContractData(8, 1, 46, 160, "LeakAddress4", x_divisor=1)]
#
#     x_sum = calculate_sum_x(data_array)
#     y_sum = calculate_sum_y(data_array)
#     x_avg = calculate_average_x(data_array)
#     y_avg = calculate_average_y(data_array)
#     x_var = calculate_variance_x(data_array)
#     y_var = calculate_variance_y(data_array)
#     x_std = standard_deviation_x(data_array)
#     y_std = standard_deviation_y(data_array)
#     x_min = calculate_minimum_x(data_array)
#     y_min = calculate_minimum_y(data_array)
#     x_max = calculate_maximum_x(data_array)
#     y_max = calculate_maximum_y(data_array)
#     x_rng = calculate_range_x(data_array)
#     y_rng = calculate_range_y(data_array)
#
#     print("x_sum: " + str(x_sum))
#     print("y_sum: " + str(y_sum))
#     print("x_avg: " + str(x_avg))
#     print("y_avg: " + str(y_avg))
#     print("x_var: " + str(x_var))
#     print("y_var: " + str(y_var))
#     print("x_std: " + str(x_std))
#     print("y_std: " + str(y_std))
#     print("x_min: " + str(x_min))
#     print("y_min: " + str(y_min))
#     print("x_max: " + str(x_max))
#     print("y_max: " + str(y_max))
#     print("x_rng: " + str(x_rng))
#     print("y_rng: " + str(y_rng))

