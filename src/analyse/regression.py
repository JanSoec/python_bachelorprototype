
class RegressionAnalyser:
    """
    This class does the actual calculation for the gradient.
    Works on an Array of SmartContractData.
    To start gradient calculation call RegressionAnalyser.calculate_gradient(SmartContractDataArray).
    """

    def calculate_average_x(self, sc_data_array):
        return self.calculate_sum_x(sc_data_array) / len(sc_data_array)

    def calculate_average_y(self, sc_data_array):
        return self.calculate_sum_y(sc_data_array) / len(sc_data_array)

    @staticmethod
    def calculate_y_intercept(x, y, slope):
        """
        Calculates the y intercept from given coordinates and slope.
        :param x: The x-Value
        :param y: The y-Value
        :param slope: Slope calculated by self.calculate_gradient()
        :return: y intercept
        """
        return y - (x * slope)

    @staticmethod
    def calculate_sum_x(sc_data_array):
        total = 0
        for element in sc_data_array:
            total += int(element.x_value.timestamp())
        return total

    @staticmethod
    def calculate_sum_y(sc_data_array):
        total = 0
        for element in sc_data_array:
            total += int(element.y_value)
        return total

    def calculate_gradient(self, sc_data_array):
        """
        :param sc_data_array: SmartContractData Array
        :return: The gradient and y intercept of the calculated line
        """
        if len(sc_data_array) <= 1:
            print("Not enough values to calculate gradient. ")
            return

        # calculate dividend
        dividend = 0
        for sc_data in sc_data_array:
            dividend += int(sc_data.x_value.timestamp()) * int(sc_data.y_value)
        x_sum = self.calculate_sum_x(sc_data_array)
        y_sum = self.calculate_sum_y(sc_data_array)
        dividend -= (y_sum * x_sum) / len(sc_data_array)

        # calculate divisor
        divisor = 0
        for element in sc_data_array:
            divisor += int(element.x_value.timestamp()) * int(element.x_value.timestamp())
        divisor -= (x_sum * x_sum) / len(sc_data_array)
        if divisor == 0:
            print("Divisor can not be 0.")
            return

        # return result
        gradient_result = dividend / divisor
        x_average = self.calculate_average_x(sc_data_array)
        y_average = self.calculate_average_y(sc_data_array)
        y_intercept = self.calculate_y_intercept(x_average, y_average, gradient_result)
        return gradient_result, y_intercept
